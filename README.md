# Welcome to petclinic environment.

## This project requires the following to be true before one can initiate the environment:
  * An s3 bucket must exist in eu-west-2, it is currently called kaddi-ass4-bucket but you can change this (details below).
  * Terraform must be installed on the machine that you want to create the infrastructure on.
  * have the AWS CLI installed, and have run the command "aws login".
This git repo contains the files to create the following:
1. A VPC in eu-west-2 which has two public and two private subnets.
   * Over the two public subnets is an ECS/FARGATE cluster, this fires up docker containers with the petclinic app on it.
   * In Public subnet 1 (in AZ A) are the NAT gateway and Jump machine.
   * Networked over the two private subnets is the RDS.
   * configuration to store state info in an s3 bucket.
   * All relevant networking configuration to enable the preceding infrastructure to exist.
2. A jenkins automation server created in the AL default public subnet in AZ C.

To see a diagram of the above, including networking configuration click the below link.
![alt text](diagram.jpg)

# Please read the entire instructions before attempting to follow them as you will need to change parameters in order to make this work.
## How to stand up entire environment:
1. Download this repo on the upping machine and navigate to the root of the downloaded copy.
2. Run the command "source /kaddi_ass4/main_code/mkenv.sh"
2. Make sure you have created a bucket with the correct name. In order to change the name of the bucket you use to do this you must:
  * change both "bucket" parameters in kaddi_ass4/main_code/bucket_for__statefile.tf
  * ensure that your bucket exists and is available to the machine you are upping from.
3. navigate to kaddi_ass4/main_code:
  * run the command "terraform init"
  * run the command "terraform plan"
  * run the command "terraform apply"
NB: You will be prompted to type "yes" in order to proceed.
NB: You will be prompted to provide a password for the Jenkins automation server, this will enable you to log into the server at http://<jenkins_public_IP>:8080 with the username "admin" and prompted password. You can find the Jenkins public IP on the AWS console.
4. Once the infrastructure is up, log in to the Jenkins automation server and build the job "start" in order to automatically pull the latest version of Petclinic, compile the app, test the compiled app and if it succeeds post the docker image to the ECR repository at which point FARGATE will grab the image to create containers. It should take a minute or two before the petclinic app is spun up once it is submitted to ECR.
5. The petclinic app should now be publicly accessible at the domain name http://kaddi.grads.al-labs.co.uk
  * In order to change which domain name you will use (You won't be able to use ours, much like the bucket) you can change the settings in kaddi_ass4/main_code/route53.tf which must be changed before you up the infrastructure.
6. Congratulations, everything should work!



## what issues might you face

1. sometimes Jenkins will time out when installing repositories, you may need to run "terraform apply" multiple times in order for this to work.

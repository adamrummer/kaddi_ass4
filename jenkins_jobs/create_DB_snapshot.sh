#!/bin/bash
# This job creates a snapshot of the below database, and is configured to trigger
# four times a day, every three hours, whilst the petclinic app is up.


# export DBENDPOINT=$(aws rds describe-db-instances \
# --region eu-west-2 \
# --db-instance-identifier kaddi-production-database \
# | jq -r '.DBInstances[].Endpoint.Address')


echo "[[[ CREATING SNAPSHOT ]]]"
aws rds create-db-snapshot \
--db-snapshot-identifier "kaddi-production-database-SNAPSHOT$(date +'%Y-%m-%d')-at-$(date +'%H')-hours" \
--db-instance-identifier "kaddi-production-database" \
--region "eu-west-2"

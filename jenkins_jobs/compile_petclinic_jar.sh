#!/bin/bash

DBCEndpoint="kaddi-production-database.cibbo9q1lrf3.eu-west-2.rds.amazonaws.com"
DBNAME="kaddiDB"
DBUSER="kaddiuser"
DBPASS="kaddipassword"

 ./mkprops ${DBCEndpoint} ${DBNAME} ${DBUSER} ${DBPASS}

export M2_HOME=/usr/local/apache-maven-3.6.0
export M2=$M2_HOME/bin
export PATH=$M2:$PATH


# sudo yum -y install maven

source /etc/profile
mvn -Dmaven.test.skip=true package

# aws s3 cp target/spring-petclinic-2.0.0.jar  s3://kaddi-super-awesome-bucket/spring-petclinic-2.0.0.jar

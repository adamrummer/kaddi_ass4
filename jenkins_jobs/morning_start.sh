#!/bin/bash

counter=0


aws ecs update-service \
--desired-count “3” \
--service kaddi-production-web

until curl -s 'http://a4-petclinic.grads.al-labs.co.uk' | grep 'PetClinic'
do
	if (( counter > 30 ))
    then
    	curl -k -X POST \
--data '{"channel": "kaddi", \
"text": "Your petclinic webservers are not up as expected!", \
"icon_url": "tps://media1.popsugar-assets.com/files/thumbor/SAxs8mTNAbkEkn0kyVK6Fg4jNj4/fit-in/1024x1024/filters:format_auto-!!-:strip_icc-!!-/2018/02/07/077/n/1922398/ce9d2113f4294ac0_GettyImages-671256244/i/When-controversial-Princess-Michael-Kent-said-interview.jpg", \
"username": "Queen Elizabeth II"}', \
https://hooks.slack.com/services/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
		exit 1

    fi
	sleep 5
    ((counter=counter+1))
done

#!/bin/bash

# Gets service name
aws ecs describe-services \
--cluster kaddi-production-ecs-cluster\


aws ecs update-service \
--desired-count "0" \
--service kaddi-production-web

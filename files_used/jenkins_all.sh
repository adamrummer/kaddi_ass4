#!/bin/bash
DBENDPOINT=$(aws rds describe-db-instances --region eu-west-2 --db-instance-identifier kaddi-production-database | jq -r '.DBInstances[].Endpoint.Address')
export DBENDPOINT
cd "$(dirname "$0")"
pwd
ls -la
cp stevesgit.sh ../
../stevesgit.sh
echo "Petclinic repo done."

./cleanup.sh
echo "Cleanup done"

cp compilejar.sh ../petclinic/
../petclinic/compilejar.sh
echo "Compiled jar file"

cp docker_compose.sh ../petclinic/
../petclinic/docker_compose.sh
echo "Composed containers"

./testinganduploading.sh
echo "Uploaded the image on ECS."

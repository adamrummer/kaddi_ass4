#!/bin/bash
#if curl localhost:2180/vets.html | grep surgery
counter=0
until curl -s 'localhost:2180' | grep 'petclinic'
do
	if (( counter > 10 ))
    then
    	"There is an error with petclinic"
    	exit 1

    fi
    sleep 10
    (( counter=counter+1 ))
done
echo "Petclinic is up!"
counter=0
until curl -s 'localhost:2180/vets.html' | grep 'surgery'
do
	if (( counter > 10 ))
    then
    	"There is an error with petclinic reaching the database"
    	exit 1

    fi
    sleep 10
    (( counter=counter+1 ))
done
echo "Petclinic is up and connected properly to the database!"

aws s3 cp s3://kaddi-ass4-bucket/version version || true
if [ ! -f version ]; then
    echo "1.0" > version
fi
version=$(cat version)
version_new=$(echo "$version + 0.1" | bc)
echo $version_new > version
$(aws ecr get-login --no-include-email --region eu-west-2)
docker tag kaddi_petclinic 109964479621.dkr.ecr.eu-west-2.amazonaws.com/kaddi:v$version_new
docker push 109964479621.dkr.ecr.eu-west-2.amazonaws.com/kaddi:v$version_new
docker tag kaddi_petclinic 109964479621.dkr.ecr.eu-west-2.amazonaws.com/kaddi:latest
docker push 109964479621.dkr.ecr.eu-west-2.amazonaws.com/kaddi:latest
aws s3 cp version s3://kaddi-ass4-bucket/version || true

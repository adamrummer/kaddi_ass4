#!/bin/bash
# Delete any previous images and containers. Don't show errors
docker rm -f kaddi-production-database.cibbo9q1lrf3.eu-west-2.rds.amazonaws.com || true
docker rm -f kaddi_petclinic || true
docker rmi -f kaddi_db || true
docker rmi -f kaddi_petclinic || true

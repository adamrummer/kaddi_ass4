#!/bin/bash
cd "$(dirname "$0")"
ls -la

if [ ! -d "petclinic" ]; then
	git clone https://catherinehillier@bitbucket.org/JangleFett/petclinic.git
else
	echo "Repo already exists. Skipping git clone"
fi
echo "Cloned Petclinic git repo."

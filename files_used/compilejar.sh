#!/bin/bash
cd "$(dirname "$0")"
DBCEndpoint=$DBENDPOINT
DBNAME="petclinic"
DBUSER="kaddiuser"
DBPASS="kaddipassword"

 ./mkprops ${DBCEndpoint} ${DBNAME} ${DBUSER} ${DBPASS}

export M2_HOME=/usr/local/apache-maven-3.6.0
export M2=$M2_HOME/bin
export PATH=$M2:$PATH


# sudo yum -y install maven

source /etc/profile
mvn -Dmaven.test.skip=true package

cp target/spring-petclinic-2.0.0.jar ../docker/web
cp src/main/resources/db/mysql/schema.sql ../docker/db/01schema.sql
cp src/main/resources/db/mysql/data.sql ../docker/db/02data.sql
# aws s3 cp target/spring-petclinic-2.0.0.jar  s3://kaddi-super-awesome-bucket/spring-petclinic-2.0.0.jar

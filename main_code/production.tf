/*====
Variables used across all modules
======*/
locals {
  production_availability_zones = ["eu-west-2a", "eu-west-2b"]
}

provider "aws" {
  region  = "${var.region}"
  #profile = "duduribeiro"
}

resource "aws_key_pair" "key" {
  key_name   = "${var.key_name}"
  public_key = "${file("kaddi.pub")}"
}

module "networking" {
  source               = "./modules/networking"
  environment          = "production"
  vpc_cidr             = "10.0.0.0/16"
  public_subnets_cidr  = ["10.0.1.0/24", "10.0.2.0/24"]
  private_subnets_cidr = ["10.0.10.0/24", "10.0.20.0/24"]
  region               = "${var.region}"
  availability_zones   = "${local.production_availability_zones}"
  key_name             = "kaddi"
  prefix               = "${var.prefix}"
  }


module "rds" {
  source            = "./modules/rds"
  environment       = "production"
  allocated_storage = "20"
  database_name     = "${var.production_database_name}"
  database_username = "${var.production_database_username}"
  database_password = "${var.production_database_password}"
  subnet_ids        = ["${module.networking.private_subnets_id}"]
  vpc_id            = "${module.networking.vpc_id}"
  instance_class    = "db.t2.micro"
  prefix            = "${var.prefix}"
}

module "ecs" {
  region              = "${var.region}"
  source              = "./modules/ecs"
  environment         = "production"
  prefix              = "${var.prefix}"
  vpc_id              = "${module.networking.vpc_id}"
  availability_zones  = "${local.production_availability_zones}"
  repository_name     = "kaddi"
  subnets_ids         = ["${module.networking.private_subnets_id}"]
  public_subnet_ids   = ["${module.networking.public_subnets_id}"]
  security_groups_ids = [
    "${module.networking.security_groups_ids}",
    "${module.rds.db_access_sg_id}"
  ]
  database_endpoint   = "${module.rds.rds_address}"
  database_name       = "${var.production_database_name}"
  database_username   = "${var.production_database_username}"
  database_password   = "${var.production_database_password}"
  secret_key_base     = "${var.production_secret_key_base}"
}
/* the task definition for the web service */
data "template_file" "populate" {
  template = "${file("populate.sh")}"
  vars {
    endpoint           = "${module.rds.rds_address}"
  }
}
resource "aws_instance" "jump" {
  ami             = "${var.image_id}"
  instance_type   = "t2.micro"
  key_name        = "kaddi"
  vpc_security_group_ids = [ "${module.rds.db_access_sg_id}", "${module.rds.security_group_jump}" ] #"al-office",
  associate_public_ip_address = true
  subnet_id = "${module.networking.public_subnets_id[0]}"
  tags {
    Name = "${var.prefix}-jump"
  }
  user_data =  "${data.template_file.populate.rendered}"
}

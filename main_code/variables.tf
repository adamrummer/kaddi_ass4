variable "prefix" {
  default = "kaddi"
}

variable "key_name" {
  default = "assess4"
}
/*====
environment specific variables
======*/

variable "production_database_name" {
  description = "The database name for Production"
}

variable "production_database_username" {
  description = "The username for the Production database"
}

variable "production_database_password" {
  description = "The user password for the Production database"
}

variable "production_secret_key_base" {
  description = "The Rails secret key for production"
}

variable "domain" {
  default = "The domain of your application"
}


variable "image_id" {
  description = "Jump machine ami"
  default = "ami-0274e11dced17bb5b"
}

variable "jenkins_password" {
  description = "This is the password for the Jenkins Server"
}

variable "region" {
  default = "eu-west-2"
}
variable "default_image_id" {
  default = "ami-0274e11dced17bb5b"
}
variable "my_tag" {
  default = "Jenkins"
  }
variable "default_subnet" {
  default = "subnet-8a08f3e3"
}

variable "PATH_TO_PRIVATE_KEY" {
default = "~/.aws/kaddi.pem"
}
variable "default_key" { default = "kaddi" }
variable "INSTANCE_USERNAME" { default = "ec2-user" }
variable "default_vpc" { default = "vpc-7738c91e" }

#jenkins
resource "aws_instance" "jenkins" {
  ami = "${var.default_image_id}"
  key_name = "${var.default_key}"
  instance_type = "t2.medium"
  tags {
    Name = "kaddi_jenkins"
    }
  # the VPC subnet
  subnet_id = "${var.default_subnet}"
  # the security group
  vpc_security_group_ids = ["${aws_security_group.allow-ssh-jenkins.id}"]
  iam_instance_profile = "JenkinsProfile"
  #user_data = "${file("./shellscripts/apacheScript.sh")}"

  provisioner "remote-exec" {
    inline = ["sudo yum -y install jq"]

    connection {
      type        = "ssh"
      user        = "${var.INSTANCE_USERNAME}"
      private_key = "${file(var.PATH_TO_PRIVATE_KEY)}"
    }
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.jenkins.public_ip} > jenkins_ip"
  }
  provisioner "local-exec" {
    command = "echo ${var.jenkins_password} > jenkins_password"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -i environments/dev site.yml"
  }
}
/* Create a file with Jenkins IP
data "template_file" "jenkins_password" {
  template = "${file("jenkins_password")}"

  vars {
    jenkins_password           = "${var.jenkins_password}"
  }

}  */

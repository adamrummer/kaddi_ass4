#!/bin/bash
#cd "$(dirname "$0")"
ls -la
cd /home/ec2-user
sudo yum install -y git mysql jq
if [ ! -d "petclinic" ]; then
	git clone https://catherinehillier@bitbucket.org/JangleFett/petclinic.git
else
	echo "Repo already exists. Skipping git clone"
fi
echo "Cloned Petclinic git repo."
DBENDPOINT=${endpoint}
export DBENDPOINT

mysql -h ${endpoint} -u kaddiuser -pkaddipassword < petclinic/src/main/resources/db/mysql/schema.sql
mysql -h ${endpoint} -u kaddiuser -pkaddipassword < petclinic/src/main/resources/db/mysql/data.sql

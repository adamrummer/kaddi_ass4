#creates security group VPC that allows ssh, jenkins and petclinic custom port
resource "aws_security_group" "allow-ssh-jenkins" {
vpc_id = "${var.default_vpc}"
name = "kaddi-allow-ssh-jenkins"
description = "security group that allows ssh and jenkins"
egress {
from_port = 0
to_port = 0
protocol = "-1"
cidr_blocks = ["0.0.0.0/0"]
}
ingress {
from_port = 22
to_port = 22
protocol = "tcp"
cidr_blocks = ["77.108.144.180/32","90.197.161.143/32"]
}

ingress {
from_port = 8080
to_port = 8080
protocol = "tcp"
cidr_blocks = ["77.108.144.180/32","90.197.161.143/32"]
}
#petclinic will be tested on jenkins on port 2180
ingress {

from_port=2180
to_port =2180
protocol="tcp"
cidr_blocks= ["77.108.144.180/32","90.197.161.143/32"]
}
tags {
Name = "kaddi-allow-ssh-jenkins"
}
}
